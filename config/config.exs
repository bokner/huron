# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for third-
# party users, it should be done in your mix.exs file.

# Sample configuration:
#
#     config :logger,
#       level: :info
#
#     config :logger, :console,
#       format: "$date $time [$level] $metadata$message\n",
#       metadata: [:user_id]

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"

config :logger, :console,
format: "\n$date $time [$level] $metadata$message",
metadata: [:user_id]

config :logger, :level, :info

# Stop lager redirecting :error_logger messages
## config :lager, :error_logger_redirect, false

# Stop lager removing Logger's :error_logger handler
## config :lager, :error_logger_whitelist, [Logger.ErrorHandler]

config :lager, crash_log: 'log/crash.log',
            crash_log_msg_size: 65536,
            crash_log_size: 10485760,
            crash_log_date: '$D0',
            crash_log_count: 5


config :huron,
      tcp_port: 5222,
      session_router: [impl: Huron.Router.Cpg, cached: false]

config :huron,
  document_handlers:
    [
      auth: {Huron.Auth, [auth_mod: Huron.NullAuth, mechanisms: ["PLAIN", "ANONYMOUS"]]},
      compress: {Huron.Compress, [methods: ["zlib"]]},
      starttls: {Huron.StartTls, [ssl_cert: "/var/huron/etc/cert.pem"]},
      message:  Huron.Message,
      presence: Huron.Presence,
      iq:       Huron.IQ
    ]

config :bootstrap,
       connect_regex: "huron.*@.*",
       connect_mode: :hidden,
       min_connections: :infinity,
       protocol: :broadcast,
       primary_port: 50337,
       secondary_ports: [50338,50339],
       ping_timeout: 5000

config :kvs,
      dba: :store_mnesia

config :cache,
      session_cache: [n: 16, ttl: 300]

config :mnesia,
   dir: 'mnesia',
   dump_log_write_threshold: 50000,
   dc_dump_limit: 40


config :riak_core,
        ## Default location of ringstate
        ring_state_dir: 'data/ring',
        platform_data_dir: './data',
			  schema_dir: 'schema',
			  wants_claim_fun:  {:riak_core_claim, :wants_claim_v3},
			  choose_claim_fun: {:riak_core_claim,:choose_claim_v3},
        handoff_port: 10029
