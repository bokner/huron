defmodule HuronXmlProcessorTest do
  use ExUnit.Case
  import Huron.XmlEl

  test "No namespace" do
    {:ok, processor} = Huron.XmlProcessor.new([{:data, fn(str) -> send(self(), str) end}])
    xmlIn = "<xml id='1'><inner from='boris'>Test</inner></xml>"
    Huron.XmlProcessor.process(processor, xmlIn)
    res = receive do
      msg -> msg
    after 0 ->
      :none
    end
    assert res == xmlIn
  end

  test "Namespace for top element" do
    {:ok, processor} = Huron.XmlProcessor.new([{:data, fn(str) -> send(self(), str) end}])
    xmlIn = "<xml xmlns='jabber:client' id='1'><inner from='boris'>Test</inner></xml>"
    Huron.XmlProcessor.process(processor, xmlIn)
    res = receive do
      msg -> msg
    after 0 ->
      :none
    end
    assert res == xmlIn
  end

    test "Namespace for inner element" do
    {:ok, processor} = Huron.XmlProcessor.new([{:data, fn(str) -> send(self(), str) end}])
    xmlIn = "<xml id='1'><inner xmlns='inner' from='boris'>Test</inner></xml>"
    Huron.XmlProcessor.process(processor, xmlIn)
    res = receive do
      msg -> msg
    after 0 ->
      :none
    end
    assert res == xmlIn
  end

    test "Namespaces for top and inner elements" do
    {:ok, processor} = Huron.XmlProcessor.new([{:data, fn(str) -> send(self(), str) end}])
    xmlIn = "<xml xmlns='outer' id='1'><inner xmlns='inner' from='boris'>Test</inner></xml>"
    Huron.XmlProcessor.process(processor, xmlIn)
    res = receive do
      msg -> msg
    after 0 ->
      :none
    end
    assert res == xmlIn
  end

  test "parsing stream:auto-reset" do
    {:ok, processor} = Huron.XmlProcessor.new(
                      [{:data, fn(str) -> send(self(), str) end},
                       {:stream_tag, "stream"}
                      ])
    xmlIn = "<stream>
    <xml xmlns='jabber:client' id='1'><inner from='boris'>Test</inner></xml></stream>"
    Huron.XmlProcessor.process(processor, xmlIn) |>
    Huron.XmlProcessor.process(xmlIn)
    res1 = receive do
      msg -> msg
    after 0 ->
      :none1
    end
    res2 = receive do
      msg -> msg
    after 0 ->
      :none2
    end
    assert res1 == res2
  end

  test "parsing document: auto-reset" do
    {:ok, processor} = Huron.XmlProcessor.new(
              [{:data, fn(str) -> send(self(), str) end}
               ])
  xmlIn = "<document xmlns='jabber:client'><element><child>Text</child></element></document>"
  Huron.XmlProcessor.process(processor, xmlIn) |>
  Huron.XmlProcessor.process(xmlIn)
  res1 = receive do
    msg -> msg
  after 0 ->
    :none1
  end
  res2 = receive do
    msg -> msg
  after 0 ->
    :none2
  end
  assert res1 == res2
  assert res1 == xmlIn
end

test "parsing stream: chunks" do
  {:ok, processor} = Huron.XmlProcessor.new(
  [{:data, fn(str) -> send(self(), str) end}
  ])
  chunk1 = "<stream:st"
  chunk2 = "ream xmlns:stream='http://etherx.jabber.org/streams'><eleme"
  chunk3 = "nt><child>Text</child></element></stream:stream>"

  p1 = Huron.XmlProcessor.process(processor, chunk1)
  assert (:none == receive do
    msg -> msg
  after 0 ->
    :none
  end)

  p2 = Huron.XmlProcessor.process(p1, chunk2)
  assert (:none == receive do
    msg -> msg
  after 0 ->
    :none
  end)

  _p3 = Huron.XmlProcessor.process(p2, chunk3)
  assert (chunk1 <> chunk2 <> chunk3 == receive do
    msg -> msg
  after 0 ->
    :none
  end)
end

test "Document-level parsing" do
  bindXml = "<iq type=\"set\" id=\"bind_1\" ><bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\"/></iq>"
  res = XmlUtils.parse(bindXml)
  ## {:xmlel, %{"id" => "bind_1", "type" => "set"},
  ##    nil,
  ##    %{"bind" => [{:xmlel, %{"xmlns" => "urn:ietf:params:xml:ns:xmpp-bind"},
  ##        nil, %{}}]}}
  xmlel(attrs: %{"id" => id, "type" => type},
          children: %{"bind" => [xmlel(attrs: %{"xmlns" => xmlns})]}) = res
  assert type == "set"
  assert id == "bind_1"
  assert xmlns ==  "urn:ietf:params:xml:ns:xmpp-bind"
end

test "Document-level serialization" do
    xml = xmlel(attrs: %{"id" => "bind_1", "type" => "set"},
                    children: %{"bind" => [xmlel(attrs: %{"xmlns" => "urn:ietf:params:xml:ns:xmpp-bind"})]})
    bindXml = "<iq type=\"set\" id=\"bind_1\" ><bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\"/></iq>"
    assert xml == XmlUtils.parse(XmlUtils.serialize("iq", XmlUtils.parse(bindXml)))
end

end
