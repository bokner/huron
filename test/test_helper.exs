defmodule TestDocumentFsm do

use Huron.XmppMacros.Document

def init(_) do
  new(:start, %{})
end

xmppDocument test_document(xmlel(children: %{"el1" => [xmlel(cdata: el1_cdata)]}), session, state) do
    send(self(), el1_cdata)
    ##IO.inspect(state)
    next_state(state, session)
end

xmppDocument test_document(_, session, state) do
  IO.puts "Unexpected custom document format"
  next_state(state, session)
end

end

ExUnit.start()
