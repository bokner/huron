defmodule HuronXmppDocTest do
  use ExUnit.Case

  test "Document handler matches the xml document" do
    {:ok, processor} = Huron.XmlProcessor.new([handler: TestDocumentFsm, data: %{}])
    xmlIn = "<test_document><el1>el1_cdata</el1><el2>e2_cdata</el2></test_document>"
    Huron.XmlProcessor.process(processor, xmlIn)
    res = receive do
      msg -> msg
    after 0 ->
      :none
    end
    assert res == "el1_cdata"
  end

  
end
