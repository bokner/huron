#!/bin/sh

OS=$(uname -s)
case $OS in
	    Linux)
		    NODE_IP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
		;;
	    Darwin)
		NODE_IP=$(ifconfig $(route get 8.8.8.8 | grep "interface: " | sed "s/[^:]*: \(.*\)/\1/") | grep "inet " | sed "s/.*inet \([0-9.]*\) .*/\1/")
		            ;;

	    *) NODE_IP=127.0.0.1
esac
SCRIPT_DIR="$(dirname "$0")"
RELEASE_ROOT_DIR="$(cd "$SCRIPT_DIR/.." && pwd)"
RELEASES_DIR="$RELEASE_ROOT_DIR/releases"
REL_NAME="huron"
REL_VSN=$(cat $RELEASES_DIR/start_erl.data | cut -d' ' -f2)
ERTS_VSN=$(cat $RELEASES_DIR/start_erl.data | cut -d' ' -f1)

sed -e "s/{{NODE_IP}}/$NODE_IP/g" $RELEASES_DIR/$REL_VSN/vm.args.template > $RELEASES_DIR/$REL_VSN/vm.args
echo "Using $RELEASES_DIR/$REL_VSN/$REL_NAME.sh"
exec "$RELEASES_DIR/$REL_VSN/$REL_NAME.sh" "$@"
