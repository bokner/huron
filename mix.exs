defmodule Huron.Mixfile do
  use Mix.Project

  def project do
    [app: :huron,
     version: "0.0.2",
     elixir: "~> 1.0",
     deps: deps,
     deps_path: "deps",
     lockfile: "mix.lock",
     build_path: "_build",
     erlc_paths: []
     ]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:exlager,
                      :crypto,
		      :syn,
		      :cpg,
                      :bootstrap,
		      :mnesia,
		      ## :kvs,
		      ## :nkbase,
		      :cache,
                      :huron_xml, :p1_tls],
     included_applications: [:ranch, :fsm, :huron_zlib, :uuid, :bisect, :eper, :recon, :tools, :exprof, :eflame, :runtime_tools, :getopt, :edown, :quickrand, :trie |
    	riak_core_apps 
     ],
     mod: {HuronMain, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}

  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [
        {:eper,  git: "git@github.com:massemanet/eper.git",    branch: "master"},
        {:recon, git: "git@github.com:ferd/recon.git", branch: "master"},
        {:exrm, github: "bitwalker/exrm", branch: "master"},
	    ## {:kvs, github: "synrc/kvs", branch: "master"},
	    ## {:nkbase, github: "ErlNETS/nkbase", branch: "master"},
        {:fsm, git: "git@github.com:bokner/fsm.git"},
	{:syn, github: "ostinelli/syn", branch: "master", compile: "rebar compile"},
	{:cpg, github: "okeuday/cpg", branch: "master"},
        {:bootstrap, github: "schlagert/bootstrap", branch: "master", compile: "rebar compile"},
	    ## {:worker_pool, git: "git@github.com:inaka/worker_pool.git", branch: "master"},
	    {:cache, github: "fogfish/cache", branch: "master", compile: "rebar compile"},
        {:exlager,   github: "khia/exlager", branch: "master"},
        {:quickrand, git: "git@github.com:okeuday/quickrand.git", branch: "master", override: true},
        {:p1_tls, git: "git@github.com:processone/tls.git",
          tag: "master",
          compile: compile_tls},
        {:huron_xml, git: "git@bitbucket.org:bokner/huron_xml.git",
            branch: "exml"},
	    {:huron_zlib, git: "git@bitbucket.org:bokner/huron_zlib.git",
	        branch: "master",
	        compile: "rebar compile"},
        {:ranch, git: "git@github.com:ninenines/ranch.git", branch: "master"},
        {:uuid, "~> 1.1"},
	    {:bisect, git: "git@github.com:knutin/bisect.git", branch: "master",
	        compile: "rebar get-deps compile"
	    },
        {:exprof, git: "git@github.com:parroty/exprof.git", branch: "master"},
	    {:eflame, git: "git@github.com:slfritchie/eflame.git", branch: "master", compile: "rebar compile"},
	    {:recon,  git: "git@github.com:ferd/recon.git", branch: "master"},
        ## Applications
 	      ## {:huron_cluster, in_umbrella: true},
        ## {:huron_xml_processor, in_umbrella: true, app: false},
        ## Overrides
	{:lager, git: "git@github.com:basho/lager.git", tag: "3.2.0", override: true},
        {:getopt, "0.8.2", override: true},
        {:edown, git: "git://github.com/uwiger/edown.git", ref: "HEAD", override: true},
        ##{:eleveldb, git: "git://github.com/basho/eleveldb.git", tag: "2.1.3", manager: :rebar, override: true},
        ##{:cuttlefish, git: "git://github.com/basho/cuttlefish.git", tag: "2.0.5", manager: :rebar, override: true}
    ]
  end

    defp riak_core_apps do
    	[]
	##[:cuttlefish, :neotoma, :setup, :sext]
    end

    defp compile_tls do
        flags = case :os.type do
        	{:unix, :darwin} -> 'CFLAGS=-I/opt/local/include LDFLAGS=-L/opt/local/lib'
  	_other -> ''
  	end
        "./configure #{flags} && make"
    end

end
