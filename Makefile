MIX = mix
.PHONY: deps

all: deps compile

compile:
	$(MIX) compile

deps:
	$(MIX) deps.get

clean:
	$(MIX) clean

distclean: clean devclean relclean
	$(MIX) deps.clean

rel: all
	$(MIX) release

relclean:
	rm -rf apps/huron/rel/huron

stage : rel
	$(foreach dep,$(wildcard deps/*), rm -rf rel/huron/lib/$(shell basename $(dep))-* && ln -sf $(abspath $(dep)) rel/huron/lib;)
	$(foreach app,$(wildcard apps/*), rm -rf rel/huron/lib/$(shell basename $(app))-* && ln -sf $(abspath $(app)) rel/huron/lib;)


##
## Developer targets
##
##  devN - Make a dev build for node N
##  stagedevN - Make a stage dev build for node N (symlink libraries)
##  devrel - Make a dev build for 1..$DEVNODES
##  stagedevrel Make a stagedev build for 1..$DEVNODES
##
##  Example, make a 68 node devrel cluster
##    make stagedevrel DEVNODES=68

.PHONY : stagedevrel devrel
DEVNODES ?= 4

# 'seq' is not available on all *BSD, so using an alternate in awk
SEQ = $(shell awk 'BEGIN { for (i = 1; i < '$(DEVNODES)'; i++) printf("%i ", i); print i ;exit(0);}')
RELEASE_NUMBER = `cat apps/huron/rel/huron/releases/start_erl.data | cut -d \  -f 2` 

$(eval stagedevrel : $(foreach n,$(SEQ),stagedev$(n)))
$(eval devrel : $(foreach n,$(SEQ),dev$(n)))


dev% : all
	mkdir -p dev/$@/huron
	cp -r apps/huron/rel/huron/* ./dev/$@/huron
	rm -rf ./dev/$@/huron/data
	rm -rf ./dev/$@/huron/log
	gen_dev $@ apps/huron/rel/huron/releases/$(RELEASE_NUMBER) ./dev/$@/huron/releases/$(RELEASE_NUMBER) 

stagedev% : dev%
	  $(foreach dep,$(wildcard deps/*), rm -rf dev/$^/lib/$(shell basename $(dep))* && ln -sf $(abspath $(dep)) dev/$^/lib;)
	  $(foreach app,$(wildcard apps/*), rm -rf dev/$^/lib/$(shell basename $(app))* && ln -sf $(abspath $(app)) dev/$^/lib;)

devclean: clean
	rm -rf dev
