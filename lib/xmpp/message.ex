defmodule Huron.Message do

  use Fsm
  import Huron.XmlEl
  require Lager

    def message(xmlel(attrs: %{"to" => to} = attrs,
                          children: children
      ),
      %{:jid => jid} = _session, :session_established, _message_opts) do
            if !is_nil(attrs["from"]) and jid != attrs["from"] do
                throw({:error, "'from' does not match the sender's jid"})
            else
                Huron.Router.route(to,
                         XmlUtils.serialize("message",
                    xmlel(attrs: Map.put(attrs, "from", jid),
                                              children: children
                          )
                ))
            end
            next_state(:session_established)
    end


end
