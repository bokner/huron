defmodule Huron.Auth do
  use Fsm
  import Huron.XmlEl

  require Lager

  def auth(
    xmlel(attrs: %{"mechanism" => mech}, cdata: authStr),
        session, :wait_for_feature_request, auth_opts) do
    {:ok, user} = (auth_opts[:auth_mod]).handle_auth(authStr, session, auth_opts[:params])
    {:ok, session} = start_auth_mech(mech, session)
    Huron.Connection.send_packet(auth_success, session)
    respond(:reset_stream, :wait_for_stream,
      Map.put(session, :authenticated, true)
      |> Map.put(:user, user)
    )
  end

  def features(opts) do
    mechanisms = opts[:mechanisms] || ["PLAIN"]
    Enum.reduce(mechanisms, "<mechanisms xmlns='urn:ietf:params:xml:ns:xmpp-sasl'>",
                            fn(m, acc) -> "#{acc}<mechanism>#{m}</mechanism>" end) <> "</mechanisms>"
  end

  defp start_auth_mech(mech, session) do
    {:ok, Map.put(session, "mech", mech)}
  end

  defp auth_success do
    "<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>"
  end

end


defmodule Huron.NullAuth do
  def handle_auth(authStr, _session, _params) do
    [_, user, _password] = :binary.split(:base64.decode(authStr), <<0>>, [:global])
    {:ok, user}
  end
end

defmodule Huron.BasicAuth do
  require Lager

  def handle_auth(authStr, _session, _params) do
    Lager.debug("Incoming auth: #{authStr}\n")
    [_, user, password] = :binary.split(:base64.decode(authStr), <<0>>, [:global])
    case check_password(user, password) do
      :ok -> {:ok, user}
      {:error, error} -> {:error, error}
    end
  end

  defp check_password(user, password) do
    ## TODO: plug in mnesia table
    :ok
  end


end
