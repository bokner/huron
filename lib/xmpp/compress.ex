defmodule Huron.Compress do

 use Fsm
 import Huron.XmlEl
 require Lager

@compress_ns "http://jabber.org/protocol/compress"

  def compress(xmlel(attrs: %{"xmlns" => @compress_ns},
      children: %{"method" =>
        [xmlel(cdata: method)]}
    ), session,
    :wait_for_feature_request, _compress_opts) do
      case upgrade_to_compressed(method) do
        {:ok, deflate_fun} ->
          send self,
            {:update_conn,
                fn(conn) ->
                   {:ok, port} = :ezlib.open()
                   Map.put(conn, :preprocess_fun,
                    fn(data) -> {:ok, d} = :ezlib.inflate(port, data); d
                    end)
                end}
          Huron.Connection.send_packet(compressed_xml, session)
          respond({:reset_stream, Huron.StreamFsm}, :wait_for_stream,
                    Map.put(session, :postprocess_fun, deflate_fun)
                    )
        {:error, error} ->
          Huron.Connection.send("#{compress_stream_failure(error)}#{XmppUtils.stream_trailer}")
          XmppUtils.close_stream(session)
          next_state(:closed)
        end
    end

    def features(opts) do
        methods = opts[:methods] || ["zlib"]
        Enum.reduce(methods, "<compression xmlns='http://jabber.org/features/compress'>",
                        fn(m, acc) -> "#{acc}<method>#{m}</method>" end) <> "</compression>"
    end

    defp compressed_xml do
      "<compressed xmlns='http://jabber.org/protocol/compress'/>"
    end

    defp compress_stream_failure(:not_supported) do
      "<failure xmlns='http://jabber.org/protocol/compress'>
        <unsupported-method/>
      </failure>"
    end

    defp compress_stream_failure(_error) do
      "<failure xmlns='http://jabber.org/protocol/compress'>
         <setup-failed/>
      </failure>"
    end

    defp upgrade_to_compressed("zlib") do
        ##z_out = :zlib.open()
        ##:ok = :zlib.deflateInit(z_out)
        {:ok, port} = :ezlib.open()
        {:ok,
            fn(data) -> {:ok, d} = :ezlib.deflate(port, data); d end
        }
    end

    defp upgrade_to_compressed(_method) do
        {:error, :not_supported}
    end

  end
