defmodule Huron.StreamFsm do
  use Fsm

  def init(%{socket: socket, transport: transport} = _tcp_session) do
    new(:wait_for_stream, %{socket: socket, transport: transport})
  end

  ## Start stream with binding
  defevent stream_start(_name, %{"to" => host, "version" => version} = attrs),
    data: %{:authenticated => true,
    :host => sess_host} = session, state: :wait_for_stream, when: host == sess_host do
      lang = XmppUtils.lang(attrs["lang"])
      {streamId, streamHeader} = XmppUtils.stream_header(host, version, lang)
      Huron.Connection.send_packet("#{streamHeader}#{Huron.Bind.bindXml}", session)
      next_state(:wait_for_bind, Map.put(session, :streamId, streamId))
  end

  ## Start stream with feature advertisement
  defevent stream_start(_name, %{"to" => host, "version" => version} = attrs), data: session do
     lang = XmppUtils.lang(attrs["lang"])
     {streamId, streamHeader} = XmppUtils.stream_header(host, version, lang)
     Huron.Connection.send_packet("#{streamHeader}#{featuresXml(host, version, lang)}", session)
     next_state(:wait_for_feature_request,
                  Map.put(session, :host, host)
                  |> Map.put(:streamId, streamId)
                  |> Map.put(:version, version)
                  |> Map.put(:lang, lang))
  end

  defevent stream_end(_name), data: session do
    XmppUtils.close_stream(session)
  end

  ## Document handler
  defevent handle_document(doc_name, xmlel), data: session, state: state do
    Huron.Xmpp.DocumentHandler.handle(doc_name, xmlel, session, state)
  end

  defevent close_session(), data: session do
    XmppUtils.close_stream(session)
    next_state(:closed)
  end

  ## Handlers, helpers

  defp featuresXml(_host, _version, _lang) do
    "<stream:features>#{Huron.Xmpp.Features.features}</stream:features>"
  end





end
