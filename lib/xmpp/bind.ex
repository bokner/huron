
  defmodule Huron.Bind do

  use Fsm
  import Huron.XmlEl
  require Lager

  def iq(
    xmlel(
      attrs: %{"id" => id, "type" => "set"},
      children: %{"bind" =>
        [xmlel(
          attrs: %{"xmlns" => "urn:ietf:params:xml:ns:xmpp-bind"},
          children: els
          )]}
  ), session, :wait_for_bind, _bind_opts) do
    ## Retrieve resource, or generate if none supplied
    resource = case els["resource"] do
      nil -> to_string(:quickrand.uniform(10000000000)) 
      [xmlel(cdata: resource_cdata)] ->
        resource_cdata
    end
    {:ok, session} = bind(resource, id, session)
    next_state(:session_established, session)
  end

  ## Helpers
  ##
  defp bind(resource, id, %{
                            :host => host,
                            :user => user
                        } = session) do
    Huron.Connection.send_packet(bindResponse(user, host, resource, id), session)
    session = Map.put(session, :resource, resource)
                |> Map.put(:jid, XmppUtils.jid(user, host, resource))
                |> Map.put(:pid, self)
    Huron.Router.register(session)
    {:ok, session}
  end

  defp bindResponse(userId, host, resource, id) do
    "<iq xmlns='jabber:client' type='result' id='#{id}' >
    <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>
    <jid>#{XmppUtils.jid(userId, host, resource)}</jid>
    </bind>
    </iq>"
  end

  def bindXml() do
    "<stream:features>
    <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/>
    <session xmlns='urn:ietf:params:xml:ns:xmpp-session'>
    <optional/>
    </session>
    </stream:features>"
  end

end
