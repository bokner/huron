defmodule Huron.Xmpp.Features do
    features_str =
    Enum.reduce(Application.get_env(:huron, :document_handlers), "",
        fn({_doc_name, module_spec}, acc) ->
          {doc_module, doc_opts} =
          case module_spec do
            {module, opts} ->
              {module, opts}
            module -> {module, []}
          end
          try do
            "#{acc}#{doc_module.features(doc_opts)}\n"
          rescue
            _ -> acc
          end
        end
    )

    def features,  do: unquote(features_str)

end