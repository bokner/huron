defmodule Huron.XmlProcessor do



  @no_stream_tag ""

  @xml_start :xml_element_start

  @xml_end :xml_element_end

  @xml_cdata :xml_cdata

  @xml_error :error

  def new(opts) do
    {:ok, port} = :exml_event.new_parser()
    handlerMod = Keyword.get(opts, :handler, Huron.DefaultHandler)
    handlerData = Keyword.get(opts, :data, :undefined)
    stream_tag = Keyword.get(opts, :stream_tag, @no_stream_tag)
    {:ok, %{handler: handlerMod,
            port: port,
            stream_tag: stream_tag,
            level: 0,
            handler_state: handlerMod.init(handlerData)}}
  end

  def reset(%{port: port} = processor) do
    reopen(port)
    %{processor | level: 0}
  end

  def process(%{port: port} = processor, xml) do
     case :exml_event.parse(port, xml) do
        {:ok, events} ->
            try do
              events |>
                Enum.reduce(processor, &processEvent/2)
              catch
                    {:halt, processor} -> processor
              end
        {:error, error} ->
                throw({:parsing_error, error, xml})
    end
  end

  defp processEvent({@xml_start, name, xmlns_list, attrs} = _event, processor) do
    processEvent({@xml_start, {name, join_xmlns(xmlns_list, attrs)}}, processor)
  end

  defp processEvent({@xml_start, {name, attrs}} = event,
      %{level: level, stream_tag: stream_tag,
        handler: handler,
        handler_state: state} = processor)
      when level == 0 and name == stream_tag do
    # Stream starts
    update_state(event, handler.stream_start(state, name, attrs), processor)
    |> Map.put(:level, level + 1)
  end

  defp processEvent({@xml_start, {name, attrs}} = _event,
      %{level: level, stream_tag: stream_tag} = processor)
      when (level == 0 and stream_tag == @no_stream_tag) or
        (level == 1 and stream_tag != @no_stream_tag) do
    # Document starts
    Huron.DocumentHandler.document_start(processor, name, attrs)
    |> Map.put(:level, level + 1)
  end

  defp processEvent({@xml_start, {name, attrs}} = _event,
  %{level: level} = processor) do
    # Element starts
    Huron.DocumentHandler.element_start(processor, name, attrs)
    |> Map.put(:level, level + 1)
  end

  defp processEvent({@xml_end, name} = event,
    %{level: level, stream_tag: stream_tag, handler: handler,
      handler_state: state} = processor)
    when level == 1 and name == stream_tag do
    # Stream ends
    update_state(event, handler.stream_end(state, name), processor)
    |> reset
  end

  defp processEvent({@xml_end, name} = event,
    %{level: level, stream_tag: stream_tag,
      port: port,
      handler: handler, handler_state: state} = processor)
    when (level == 1 and stream_tag == @no_stream_tag)
          or (level == 2 and stream_tag != @no_stream_tag) do
    # Document ends
    processor = Map.put(processor, :level, level - 1)
    ## Reset the stream if level is going to 0 (stream level)
    :ok =  case level do 1 -> reopen(port); _ -> :ok end
    document = Huron.DocumentHandler.get_document(processor)
    update_state(event, handler.handle_document(state, name, document), processor)
  end

  defp processEvent({@xml_end, name} = _event,
    %{level: level} = processor)
    when level > 1 do
    # Element ends
    Huron.DocumentHandler.element_end(processor, name)
    |> Map.put(:level, level - 1)
  end

  defp processEvent({@xml_cdata, cdata} = _event,
    processor) do
      case String.strip(cdata) do
        "" ->
          ## Skip whitespaced cdata
          processor
        d ->
          Huron.DocumentHandler.cdata(processor, d)
      end
  end

  defp processEvent({@xml_error, error}, _processor) do
    throw({:xml_error, error})
  end

  # Utility functions
  defp reopen(port) do
    :ok = :exml_event.reset_parser(port)
  end

  defp update_state(event, result, processor) do
    case result do
      :ignore ->
        processor
      {:reset_stream, newState} ->
        processor = reset(%{ processor | handler_state: newState})
        throw({:halt, processor})
      {{:reset_stream, newHandler}, newState} ->
        processor = reset(%{ processor | handler_state: newState, handler: newHandler})
        throw({:halt, processor})
      {{:delegate, newHandler}, newState} ->
        processEvent(event,
          %{ processor | handler_state: newState, handler: newHandler}
        )
      {{:switch, newHandler}, newState} ->
        %{ processor | handler_state: newState, handler: newHandler}
      {newHandler, newState} ->
        %{ processor | handler_state: newState, handler: newHandler}
      newState ->
        %{ processor | handler_state: newState}
    end
  end


  defp join_xmlns(xmlns_list, attrs) do
    Enum.reduce(xmlns_list, attrs, fn({prefix, uri}, map) -> Map.put(map, ns_name(prefix), uri) end)
  end

  defp ns_name(:none) do
    "xmlns"
  end

  defp ns_name(prefix) do
     "xmlns:#{prefix}"
  end
end
