defmodule Huron.StartTls do

  use Fsm
  import Huron.XmlEl
  require Lager

  @tls_ns "urn:ietf:params:xml:ns:xmpp-tls"

  def starttls(xmlel(attrs: %{"xmlns" => @tls_ns}),
    %{transport: transport, socket: socket} = session,
    :wait_for_feature_request, starttls_opts) do
    case upgrade_to_tls(transport, socket, starttls_opts) do
      {:ok, tlsSocket} ->
        Huron.Connection.send_packet(proceed_tls_xml, session)
        send self, {:update_conn, fn(conn) ->
                                                Map.put(conn, :socket, tlsSocket) |>
                                                Map.put(:transport, :p1_tls)
                                                end}
        respond({:reset_stream, Huron.StreamFsm}, :wait_for_stream,
                  Map.put(session, :transport, :p1_tls) |>
                  Map.put(:socket, tlsSocket)
                  )
      {:error, Error} ->
        Huron.Connection.send_packet("#{tls_stream_failure}#{XmppUtils.stream_trailer}", session)
        XmppUtils.close_stream(session)
        next_state(:closed)
      end
  end

  def features(_opts) do
    "<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>"
  end

  ## Upgrade to TLS
  ## Returns {ok, tlsSocket} or {error, Reason}
 
  defp upgrade_to_tls(transport, socket, opts) when transport == :ranch_tcp do
      tlsOpts = [{:verify, :verify_none}, {:certfile, get_certfile(opts)}
      | Application.get_env(:p1_tls, :ssl_options, [])]
      :p1_tls.tcp_to_tls(socket, tlsOpts)
  end

  defp upgrade_to_tls(transport, socket, _opts) when transport == :p1_tls do
    {:ok, socket}
  end

  defp get_certfile(opts) do
      case opts[:ssl_cert] do
        nil -> :filename.join([:code.priv_dir(:huron), 'cert.pem'])
        certfile -> certfile
      end
  end

  defp proceed_tls_xml do
    "<proceed xmlns='#{@tls_ns}'/>"
  end

  defp tls_stream_failure do
    "<failure xmlns='#{@tls_ns}'/>"
  end

end
