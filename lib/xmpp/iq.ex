defmodule Huron.IQ do

  import Fsm
  import Huron.XmlEl
  require Lager

  def iq(xmlel, session, :wait_for_bind, bind_opts) do
    Huron.Bind.iq(xmlel, session, :wait_for_bind, bind_opts)
  end

  def iq(xmlel(attrs: %{"type" => "set", "id" => id},
                        children: %{"session" =>
                          [xmlel(attrs: %{"xmlns" => "urn:ietf:params:xml:ns:xmpp-session"})]}
    ), session, :session_established, _iq_opts) do
          Huron.Connection.send_packet(XmppUtils.replyIq(id), session)
          next_state(:session_established)
  end

  def iq(xmlel(attrs: %{"type" => "get", "id" => id}),
    session, :session_established, _iq_opts) do
    ## Temporary : respond with empty iq result
    Huron.Connection.send_packet(XmppUtils.replyIq(id), session)
    next_state(:session_established)
  end



end
