defmodule Huron.Presence do

  use Fsm
  import Huron.XmlEl
  require Lager


  ## Presence
  def presence(xmlel(attrs: attrs),
          %{
            user: user,
            host: host,
            resource: resource
          } = session, :session_established, _presence_opts) do
    case attrs["type"] do
      type when is_nil(type) or type == "available" ->
        Huron.Connection.send_packet(XmppUtils.presence(:available, user, host, resource), session)
        "unavailable" ->
          :todo
    end
    next_state(:session_established)
  end

end
