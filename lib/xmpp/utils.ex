defmodule XmppUtils do

  require Lager

  def close_stream(session) do
    Huron.Router.unregister(session)
    case session[:pid] do
	nil -> :ok
	pid -> send pid, :close_stream
    end
  end

  def make_send_fun(module, socket) do
    fn(data) ->
        module.send_packet(socket, data)
    end
  end

  def make_send_fun(module, socket, post_process) do
    fn(data) ->
     case post_process.(data) do
        {:ok, data1}-> module.send_packet(socket, data1)
        {:error, error} -> {:error, error}
     end
    end
  end


  def make_send_fun_global(fun) do
    fun
    ##origin_node = node
    ##fn(data) ->
    ##    :erlang.spawn(origin_node, :erlang, :apply, [fun, [data]])
    ##end
  end


  def zlib_compress(data) do
    Lager.info("Compressing #{data}")
  	z = :zlib.open()
  	:zlib.deflateInit(z)
  	try do
  	    :zlib.deflate(z, data, :full)
  	after
  	   Lager.info("Closing zlib")
  	   :zlib.close(z)
  	end
  	##:zlib.deflateEnd(z)
  	##:erlang.list_to_binary([b1])
  end

  def zlib_uncompress(data) do
  	z = :zlib.open()
  	:zlib.inflateInit(z)
  	try do
  	    :zlib.inflate(z, data)
  	after
  	    :zlib.close(z)
  	end
  end


  def stream_header(host, version, lang) do
    streamId = :quickrand.uniform(10000000000) 
    {streamId,
    "<stream:stream xmlns='jabber:client'
    xmlns:stream='http://etherx.jabber.org/streams'
    id='#{streamId}'
    from='#{host}'
    version='#{version}'
    xml:lang='#{lang}'>"}
  end

  def stream_trailer do
    "</stream:stream>"
  end

  def replyIq(id) do
    "<iq id='#{id}' type='result'/>"
  end

  def presence(:available, user, host, resource) do
    "<presence from='#{jid(user, host)}' to='#{jid(user, host, resource)}'>
    </presence>"
  end

  def message(attrs, body) do
    "<mesage #{XmlUtils.attrs_to_iolist(attrs)}>
    <body>#{body}</body>
    </message>"
  end

  def jid(user, host) do
    "#{user}@#{host}"
  end

  def jid(user, host, resource) do
    "#{user}@#{host}/#{resource}"
  end

  def parse_jid(jid) do
    case :binary.split(jid, ["/", "@"], [:global]) do
      # Full JID
      [user, host, resource] -> {user, host, resource}
      # Bare JID
      [user, host] -> {user, host}
      # Host
      [host] -> host
    end
  end

  def bare_jid(jid) when is_binary(jid) do
    bare_jid(parse_jid(jid))
  end

  def bare_jid(jid) do
    case jid do
      {host, user, _resource} -> {host, user}
      {host, user} -> {host, user}
    end
  end

  def lang(nil) do
    "en"
  end

  def lang(l) do
    l
  end


  def handler_params(doc_name) when is_binary(doc_name) do
    handler_params(String.to_atom(doc_name))
  end

  def handler_params(doc_name) when is_atom(doc_name) do
    case Application.get_env(:huron, :document_handlers)[doc_name] do
      nil -> throw({:document_handler_not_found, doc_name})
      {_module, params} -> params
      _module ->  []
    end
  end
end
