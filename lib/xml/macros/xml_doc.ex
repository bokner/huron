

  ##
  ## Document-level handler
  ##
defmodule  Huron.XmppMacros.Document do
  import Fsm

  defmacro __using__(_options) do
    quote do
      use Fsm
      import Huron.XmlEl
      import unquote(__MODULE__)
      ## The attribute prevents from duplicate generation of shared AST
      Module.register_attribute __MODULE__, :ast_generated,
        ## accumulate: true,
         persist: false
      @ast_generated nil
    end
  end

  defmacro xmppDocument(head, body)  do
    {doc_name, args_ast} = name_and_args(head)
    {_arg_names, decorated_args} = decorate_args(args_ast)
    head = Macro.postwalk(head,
    fn
      ({fun_ast, context, old_args}) when (
        fun_ast == doc_name and old_args == args_ast
      ) ->
        {fun_ast, context, decorated_args}
      (other) -> other
    end)

    doc_name_str = to_string(doc_name)


    ast_main = quote do

      
      defp unquote(head) do
        unquote(body[:do])
      end
      #
      defevent document_start(unquote(doc_name_str) = name,  attrs), state: state, data: session do
            next_state(state, Huron.DocumentHandler.document_start(session, name,  attrs))
      end

      defevent document_start(name, _attrs), state: state, data: session do
            throw {:unexpected_document, name, expected: unquote(doc_name_str)}
      end

      defevent document_end(unquote(doc_name_str) = name), state: state, data: session do
        unquote(doc_name)(Huron.DocumentHandler.get_document(session),
          Huron.DocumentHandler.document_end(session, name), state)
      end

      defevent document_end(_name), state: state, data: session do
        next_state(state, session)
      end

    end



    ast_shared = quote do
      @ast_generated true
      defevent element_start(name,  attrs), state: state, data: session do
        next_state(state, Huron.DocumentHandler.element_start(session, name,  attrs))
      end

      defevent element_end(name), state: state, data: session do
        next_state(state, Huron.DocumentHandler.element_end(session, name))
      end

      defevent cdata(data), state: state, data: session do
        next_state(state, Huron.DocumentHandler.cdata(session, data))
      end

      defevent stream_end(_name), data: session do
        XmppUtils.close_stream(session)
        next_state(:closed)
      end


    end

    ##IO.inspect(Macro.to_string(Macro.expand(ast, __ENV__)))

    quote do
      unquote(ast_main)
      if !@ast_generated do
        unquote(ast_shared)
      end
    end

  end

  defp name_and_args({:when, _, [short_head | _]}) do
      name_and_args(short_head)
    end

  defp name_and_args(short_head) do
      Macro.decompose_call(short_head)
  end

  defp decorate_args([]), do: {[],[]}

  defp decorate_args(args_ast) do
    for {arg_ast, index} <- Enum.with_index(args_ast) do
      # dynamically generate quoted identifier
      arg_name = Macro.var(:"arg#{index}", __MODULE__)

      # generate AST for patternX = argX
      full_arg = quote do
        unquote(arg_ast) = unquote(arg_name)
      end

      {arg_name, full_arg}
    end
    |> :lists.unzip
  end



end
