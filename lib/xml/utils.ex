defmodule XmlUtils do
  import Huron.XmlEl
  require Record



  def attrs_to_iolist(map) when :erlang.is_map(map) do
    Enum.reduce(map, "", fn({name, value}, acc) -> " " <> name <> "='" <> value <> "'" <> acc end)
  end

  def parse(xml) do
    {:ok, processor} = Huron.XmlProcessor.new([handler: Huron.DocumentHandler,
         data: fn(_state, _doc_name, doc) -> send(self(), doc) end])
     Huron.XmlProcessor.process(processor, xml)
     receive do
       doc -> doc
     after 0 ->
       :none
     end
  end

  def serialize(name, xmlel) do
    :exml.serialize(name, xmlel)
  end

  def _serialize(name, xmlel) do
    to_iolist(name, xmlel)
  end

  defp to_iolist(name, xmlel(attrs: attrs) = xml) when Record.is_record(xmlel, :xmlel) do
      "<#{name}#{attrs_to_iolist(attrs)}>#{to_iolist(xml)}</#{name}>"
  end

  defp to_iolist(xmlel(cdata: cdata, children: children)) when Record.is_record(xmlel, :xmlel) do
    "#{cdata}#{to_iolist(children)}"
  end

  defp to_iolist(children) when is_map(children) do
    Enum.reduce(children, "",
        fn({el_name, el_children}, acc) ->
            Enum.reduce(el_children, acc,
                    fn(c, acc2) ->
                        "#{acc2}#{to_iolist(el_name, c)}"
                    end)
        end)
  end

end
