defmodule Huron.XmlEl do
  require Record
  Record.defrecord :xmlel, [attrs: %{}, cdata: nil, children: %{}]
end
