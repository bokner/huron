defmodule Huron.DocumentHandler do
    import Huron.XmlEl

    def init(callback) do
      %{callback: callback}
    end

    def stream_start(state, _name, _attrs) do
      state
    end

    def stream_end(state, _name) do
      state
    end

    def document_start(state, _name, attrs) do
      Map.put(state, :_xmldoc_, [xmlel(attrs: attrs)])
    end

    def document_end(state, doc_name) do
      if state[:callback] do
        (state[:callback]).(state, doc_name, get_document(state))
      end
      Map.delete(state, :_xmldoc_)
    end

    def element_start(state, _name, attrs) do
      Map.put(state, :_xmldoc_, [xmlel(attrs: attrs) | state[:_xmldoc_]])
    end

    def element_end(state, name) do
      case state[:_xmldoc_] do
        [el, parent | rest] ->
          p = xmlel(parent,
            children: Map.update(xmlel(parent, :children), name, [el],
              fn(els) -> [el | els] end))
          Map.put(state, :_xmldoc_, [p | rest])
        _ -> state
      end
    end

    def cdata(state, data) do
      case state[:_xmldoc_] do
        [el | rest] ->
          e = xmlel(el, cdata: data)
          Map.put(state, :_xmldoc_, [e | rest])
        _ -> state
      end
    end

    def get_document(state) do
      case state[:_xmldoc_] do
        [el | _rest] -> el
        _ -> nil
      end
    end

  end
