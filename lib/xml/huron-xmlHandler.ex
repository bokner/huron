defmodule Huron.XmlHandler do
  use Behaviour

  defcallback init(data :: any) :: any

  defcallback stream_start(handlerState :: any, name :: binary,  attrs :: list) :: any

  defcallback document_start(handlerState :: any, name :: binary, attrs:: list) :: any

  defcallback document_end(handlerState :: any, name :: binary) :: any

  defcallback stream_end(handlerState :: any, name :: binary) :: any

  defcallback element_start(handlerState :: any, name :: binary,  attrs :: list) :: any

  defcallback element_end(handlerState :: any, name :: binary) :: any

  defcallback cdata(handlerState :: any, cdata :: binary) :: any

end
