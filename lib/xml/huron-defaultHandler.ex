defmodule Huron.DefaultHandler do

    def init(callback) do
      %{callback: callback, xml: ""}
    end

    def stream_start(state, name, attrs) do
      Map.put(state, :xml, "#{state[:xml]}<#{name}#{attrs_to_iolist(attrs)}>")
    end

    def stream_end(state, _name) do
      state
    end

    def document_start(state, name, attrs) do
      Map.put(state, :xml, "<#{name}#{attrs_to_iolist(attrs)}>")
    end

    def document_end(state, name) do
      (state[:callback]).("#{state[:xml]}</#{name}>")
      state
    end

    def element_start(state, name, attrs) do
      Map.put(state, :xml, "#{state[:xml]}<#{name}#{attrs_to_iolist(attrs)}>")
    end

    def element_end(state, name) do
      Map.put(state, :xml, "#{state[:xml]}</#{name}>")
    end

    def cdata(state, data) do
      Map.put(state, :xml, "#{state[:xml]}#{data}")
    end

    def attrs_to_iolist(map) when :erlang.is_map(map) do
      XmlUtils.attrs_to_iolist(map)
    end

  end
