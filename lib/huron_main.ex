defmodule HuronMain do

  require Lager
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    join_cluster
    children = [
      # Define workers and child supervisors to be supervised
      # worker(Huron.Worker, [arg1, arg2, arg3])
      router_sup(), ranch_sup() | ranch_listeners()
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Huron.Supervisor]
    {:ok, _pid} = Supervisor.start_link(children, opts)
  end

  def ranch_sup() do
    {:ranch_sup, {:ranch_sup, :start_link, []},
    :permanent, 5000, :supervisor, [:ranch_sup]}
  end

  # TODO: more options and configurable port and pool size
  def ranch_listeners() do
    [:ranch.child_spec(Huron.Connection, 10, :ranch_tcp,
    [{:port, Application.get_env(:huron, :tcp_port, 3222)},
    {:max_connections, :infinity}
    ],
    Huron.Connection, [{:session_handler, Huron.Xmpp.SessionHandler}])]
  end

  def router_sup() do
    {:router_sup, {Huron.Router, :start_link, []},
    :permanent, 5000, :supervisor, [:router_sup]}
  end

  def join_cluster do
    Lager.info("Looking for cluster nodes...")
    Lager.info("Cluster: #{inspect Huron.Cluster.join}")
  end

end
