defmodule Huron.Cluster do
    require Lager
    def cluster? do
      {:ok, self_node, connected_nodes, _handlers} = :bootstrap_lib.get_info
      case connected_nodes do
        [] -> false
        nodes when is_list(nodes) ->
            case Enum.filter(nodes, fn(n) -> n != self_node end) do
                [] -> false
                peer_nodes -> peer_nodes
            end
        _ -> false
      end
    end

    def join do
        if !bootstrap do
            bootstrap
        else
            cluster?
        end
    end


    def bootstrap do
        bootstrap(:application.get_env(:bootstrap, :ping_timeout, 10000))
    end

    def bootstrap(timeout) do
        :bootstrap.monitor_nodes(true)
        res = cluster_discovery_loop(timeout)
        :bootstrap.monitor_nodes(false)
        res
    end


    def cluster_discovery_loop(timeout) do
    receive do
	    {:bootstrap, {:nodeup, n}} when n == :erlang.node() ->
	        cluster_discovery_loop(timeout)
	    {:bootstrap, {:nodedown, _node, _reason}} ->
	        cluster_discovery_loop(timeout);
	    {:bootstrap, {:nodeup, _n}} ->
	        cluster?
        msg -> msg
    after timeout ->
	    cluster? 
	end
  end

end
