defmodule Huron.Router do
    require Lager
    router_opts = Application.get_env(:huron, :session_router, [impl: Huron.Router.Ets, cached: true])

    def impl_module, do: unquote(router_opts[:impl])

    def cache?, do: unquote(router_opts[:cached])

    ## Implementation
    def start_link, do: impl_module.start_link

    def register(%{user: user,
                   host: host,
                   resource: resource} = session) do
     cache? && Huron.Cache.uncache_session({user, host, resource})
     impl_module.register(
        %{user: user,
          host: host,
          resource: resource,
          socket: session[:socket],
          transport: session[:transport],
          postprocess_fun: session[:postprocess_fun],
          pid: session[:pid] || self
          }
     )
    end

    def unregister(%{user: user,
                     host: host,
                     resource: resource} = session) do
     cache? && Huron.Cache.uncache_session({user, host, resource})
     impl_module.unregister(session)
    end

    def unregister(_), do: :ok

    def find_by(jid) when is_binary(jid) do
        find_by(XmppUtils.parse_jid(jid))
    end

    def find_by(jid) do
       if cache? do
        Huron.Cache.get(:session_cache, jid, &impl_module.find_by/1)
       else
        impl_module.find_by(jid)
       end
    end

    def session_count do
        impl_module.session_count
    end

  def route(jid, data) when is_binary(jid) do
    route(XmppUtils.parse_jid(jid), data)
  end

  def route(jid, data), do: route(jid, data, &Huron.Router.find_by/1)

  def route(jid, data, get_recipients_fun) do
      Lager.debug("Routing #{data}")
      case get_recipients_fun.(jid) do
        [] ->
          {:not_found, jid}
        sessions ->
            Enum.each(sessions,
              fn(session) ->
               impl_module.route(session, data)
              end)
            :ok
      end
  end

end
