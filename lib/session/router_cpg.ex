defmodule Huron.Router.Cpg do
  @moduledoc false
  require Lager

  def start_link do
    :cpg.start_link(:session)
    :cpg.start_link(:bare)
  end

  def register(%{user: user, host: host, resource: resource, pid: pid} = _session) do
    :cpg.join(:session, jid_key(user, host, resource), pid)
    :cpg.join(:bare, jid_key(user, host), pid)
  end

  def unregister(_session) do
    :ok
  end

  ## Find session by full jid
  def find_by({user, host, resource} = _jid) do
      case :cpg.get_closest_pid(:session, jid_key(user, host, resource)) do
        {:ok, _, session_pid} -> [session_pid]
        {:error, {:'no_such_group', _}} -> []
      end
  end

  ## Find sessions by bare jid
  def find_by({user, host} = _jid) do
     case :cpg.get_members(:bare, jid_key(user, host)) do
       {:ok, _, session_pids} -> session_pids
       {:error, {:'no_such_group', _}} -> []
     end
  end

  def session_count do
    Enum.reduce(:cpg.which_groups(:session), 0, fn(group, acc) ->  acc + :cpg.join_count(:session, group) end)
  end

  def route(session, data) when is_pid(session) do
    send session, {:route, data}
  end

  defp jid_key(user, host, resource) do
    to_char_list(XmppUtils.jid(user, host, resource))
  end

  defp jid_key(user, host) do
    to_char_list(XmppUtils.jid(user, host))
  end


end
