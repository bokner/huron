defmodule Huron.Router.NkBase do

  require Lager
  def start_link do
    Supervisor.start_link(__MODULE__, [])
  end

  def init([]) do
    init
    {:ok, {{:one_for_one, 1, 1}, []}}
  end

  def init do
    :nkbase.register_class(:huron, :session,
      %{
        backend: :ets,
        n: 3,
        indices: [
          i_jid: {:func, fn(_ext_key, session) ->
                            {session[:user], session[:host], session[:resource]}
                         end
                  },
          i_bare: {:func, fn(_ext_key, session) ->
                            {session[:user], session[:host]}
                         end
                  },
      ]})
    end

  def register(session) do
    :ok = :nkbase_dmap.update(:huron, :session, session[:jid],
            [
              user: {:assign, session[:user]},
              host: {:assign, session[:host]},
              resource: {:assign, session[:resource]},
              send_data_fun: {:assign, session[:send_data_fun]}
            ]
    )
  end

  def unregister(session) do
    :ok = :nkbase.del(:huron, :session, session[:jid])
  end

  def find_by({user, host, resource} = _jid) do
    case :nkbase.search(:huron, :session,
                    [
                      {:i_jid, {:eq, {user, host, resource}}}
                    ],
                    %{get_fields: [:send_data_fun]}) do
      {:ok, sessions} ->
        sessions
      error ->
        error
    end
  end

  def find_by({user, host} = _barejid) do
    case :nkbase.search(:huron, :session,
                    [
                      {:i_bare, {:eq, {user, host}}}
                    ],
                    %{get_fields: [:send_data_fun]}) do
      {:ok, sessions} ->
        sessions
      error ->
        error
    end
  end


  def route(jid, data) do
    route(jid, data, fn(jid) -> Huron.Router.NkBase.find_by(jid) end)
  end

  def route(jid, data, find_recipients_fun) do                                                      
      Lager.debug("Routing #{data}")                                                                
      case find_recipients_fun.(jid) do                                                             
        [] ->                                                                                       
          {:not_found, jid}
        sessions when is_list(sessions) ->                                                          
            Enum.each(sessions,                                                                     
              fn({_key1, _key2, [session]} = _session) ->                                           
                send_data = session[:fields][:send_data_fun]                                        
                send_data.(data)
              end)
            :ok
        error ->                                                                                    
          error                                                                                     
      end                                                                                           
  end                                                                                               
                                                                                                    
  
end 

