defmodule Huron.Router.Ets do
  require Lager
  @ring_size 64

  def start_link do
    Agent.start_link(fn ->
     Enum.each(1..@ring_size,
     fn(p) ->
      :ets.new(partition_table(p-1), [:ordered_set, :named_table, :public, read_concurrency: true, write_concurrency: true])
     end)
    end)
  end


  def register(%{
		    user: user, 
		    host: host,
            resource: resource
                    } = session) do
    {partition, _key} = hash_key(user, host)
    :ets.insert(partition_table(partition), {
        {host, user, resource},
        session
      })
  end

  def unregister(%{user: user,
                  host: host,
                  resource: resource} = _session) do
    {partition, _key} = hash_key(user, host)
    :ets.delete(partition_table(partition), {host, user, resource})
  end


  def find_by(jid) do
    sessions = case jid do
      {user, host, resource} ->
	{partition, _key} = hash_key(user, host)
        :ets.lookup(partition_table(partition), {host, user, resource})
    {user, host} ->
        {partition, _key} = hash_key(user, host)
	    sessionTab = partition_table(partition)
        ## :ets.fun2ms(fn({{jid, _}, session} = obj) -> obj end)
        match = [{{{host, user, :_}, :"$1"}, [], [:"$_"]}]
        :ets.select(sessionTab, match)
    end
    Enum.map(sessions, fn({_key, session}) -> session end)
  end


  def route(jid, data) do
 	  route(jid, data, &__MODULE__.find_by/1)
  end

  def route(jid, data, find_recipients_fun) when is_binary(jid) do
    route(XmppUtils.parse_jid(jid), data, find_recipients_fun)
  end

  def route(jid, data, find_recipients_fun) do
      Lager.debug("Routing #{data}")
      case find_recipients_fun.(jid) do
        [] ->
          {:not_found, jid}
        sessions ->
            Enum.each(sessions,
              fn(session) ->
                Huron.Connection.send_packet(data, session)
              end)
            :ok
      end
  end

  defp hash_key(user, host) do
     user_key = :crypto.hash(:sha, :erlang.term_to_binary({host, user}))
     {Huron.CHash.partition(user_key, @ring_size), user_key} 
  end

  defp partition_table(partition) do 
     :"session.#{partition}"
  end

end

