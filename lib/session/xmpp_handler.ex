defmodule Huron.Xmpp.SessionHandler do

  def init(session) do
    {:ok, processor} = Huron.XmlProcessor.new([handler: Huron.StreamFsm,
                        data: session,
                        stream_tag: "stream:stream"
                        ])
        Map.put(session, :xml_processor, processor)
  end

  def handle_incoming(data, session) do
    Map.put(session, :xml_processor,
      Huron.XmlProcessor.process(session[:xml_processor], data)
      )
  end

  def cleanup(%{xml_processor: %{handler_state: handler_state, handler: stream_handler} = _processor} = _session) do
    stream_handler.close_session(handler_state)
  end
end

## This module stub should be regenerated from the configuration
defmodule Huron.Xmpp.DocumentHandler do
    Enum.each(Application.get_env(:huron, :document_handlers),
        fn({doc_name, module_spec}) ->
          {doc_module, doc_opts} =
          case module_spec do
            {module, opts} ->
              {module, opts}
            module -> {module, []}
          end
          def handle(unquote("#{doc_name}"), xmlel, session, state), do: unquote(doc_module).unquote(doc_name)(xmlel, session, state, unquote(doc_opts))
        end
    )

    def handle(doc_name, _xmlel, _session, _state) do
        throw({:no_document_handlers, doc_name})
    end
end
