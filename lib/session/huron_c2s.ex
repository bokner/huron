defmodule Huron.Connection do
  use GenServer
@moduledoc """
Module that implements ranch_protocol callbacks for TCP connection.
'opts' have additional parameters (currently only session_handler)
"""

@type conn :: map()
@type socket :: port()
@type ref :: ref()

@behaviour :ranch_protocol

  require Lager

  def start_link(ref, socket, transport, opts) do
    :proc_lib.start_link(__MODULE__, :init, [ref, socket, transport, opts])
    ##{:ok, spawn_link fn -> init(ref, socket, transport, opts) end}
  end


  def init(ref, socket, transport, opts) do
    :quickrand.seed
    :ok = :proc_lib.init_ack({:ok, self()})
    ## Perform any required state initialization here.
    :ok = :ranch.accept_ack(ref)
    conn = new_conn(ref, socket, transport, opts)
    transport.setopts(socket, [{:active, :once}])
    :gen_server.enter_loop(__MODULE__, [], conn)
  end

  def handle_info({:tcp, sock, data}, %{socket: socket, transport: :ranch_tcp} = conn)
            when sock == socket do
      {:noreply, handle_raw(data, conn)}
  end

  def handle_info({:tcp, sock, data},
        %{socket: {:tlssock, socket, _Port} = tlsSocket, transport: :p1_tls} = conn)
            when sock == socket do
      case :p1_tls.recv_data(tlsSocket, data) do
	     {:ok, tlsData} ->
	        {:noreply, handle_raw(tlsData, conn)}
         {:error, reason} ->
            {:stop, {:tls_error, reason}, conn}
      end
  end

  def handle_info({:route, data}, conn) do
    send_packet(data, conn)
    {:noreply, conn}
  end

  def handle_info({:update_conn, fun}, conn) do
    {:noreply, fun.(conn)}
  end

  def handle_info({:tcp_closed, _socket}, conn) do
       Lager.debug("Connection closed")
       {:stop, :normal, conn}
  end

  def handle_info({:tcp_error, _socket, reason}, conn) do
        :ok = close_conn(conn)
        Lager.error("Transport error: #{reason}")
    	 {:stop, :normal, conn}
  end

  def handle_info(:close_stream, conn) do
      :ok = close_conn(conn)
      {:stop, :normal, conn}
  end

  # Ignore {Ref, :ok} message that would come from ranch on initialization
  def handle_info({ref, :ok}, conn) when is_reference(ref) do
    {:noreply, conn}
  end

  def handle_info(unknown, conn) do
    Lager.warning("Unknown message to conn process: #{inspect(unknown)}")
    {:noreply, conn}
  end

  def terminate(reason, conn) do
    cleanup(conn)
    case reason do
      :normal ->
        :ok
      _ ->
        Lager.error("Terminated: #{inspect(reason)}\n State: #{inspect(conn)}")
      end
    :ok
  end

  def close_conn(%{transport: transport, socket: socket} = _conn) do
    transport.close(socket)
  end

  @spec new_conn(ref, socket, conn, Keyword) :: conn

  defp new_conn(ref, socket, transport, opts) do
    connHandler = Keyword.get(opts, :session_handler)

    connHandler.init(%{
        ref: ref,
        socket: socket,
         transport: transport,
         session_handler: connHandler
        })
  end


  ## Handling raw incoming socket data
  def handle_raw(data, %{transport: transport, socket: socket} = conn) do
      ## apply pre-processing (deflate etc)
      data = case conn[:preprocess_fun] do
        nil -> data
        fun -> fun.(data)
      end

      try do
        handle_data(data, conn)
      after
        ## Prepare the socket for next data portion
        transport.setopts(socket, [{:active, :once}])
      end
  end

  ## Sending data over the socket
  def send_packet(data, %{transport: transport, socket: socket} = conn) do
    data = case conn[:postprocess_fun] do
                   nil -> data
                   fun -> fun.(data)
                 end
    transport.send(socket, data)
  end

  ##
  ## conn handler is a module, and can be dynamically changed
  ## by the current conn handler
  ##
  @spec handle_data(any, conn) :: conn

  defp handle_data(data, %{session_handler: handler} = conn)
    when is_atom(handler) do
      handler.handle_incoming(data, conn)
  end

  defp cleanup(conn) do
    conn[:session_handler].cleanup(conn)
  end

  end
