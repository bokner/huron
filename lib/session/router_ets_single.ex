defmodule Huron.Router.Ets.Single do
  require Lager

  def start_link do
    Agent.start_link(fn ->
      :ets.new(:session, [:ordered_set, :named_table, :public, read_concurrency: true, write_concurrency: true])
    end)
  end


  def register(%{
		    user: user, 
		    host: host,
            resource: resource
                    } = session) do
    :ets.insert(:session, {
        {host, user, resource},
        session
      })
  end

  def unregister(%{user: user,
                  host: host,
                  resource: resource} = _session) do
    :ets.delete(:session, {host, user, resource})
  end

  def unregister(_session) do
    :ok
  end

  def find_by(jid) do

    sessions = case jid do
      {user, host, resource} ->
        :ets.lookup(:session, {host, user, resource})
      {user, host} ->
        ## :ets.fun2ms(fn({{jid, _}, session} = obj) -> obj end)
        match = [{{{host, user, :_}, :"$1"}, [], [:"$_"]}]
        :ets.select(:session, match)
    end
    Enum.map(sessions, fn({_key, session}) -> session end)
  end


  def route(jid, data) do
 	  route(jid, data, &Huron.Router.find_by/1)
  end

  def route(jid, data, find_recipients_fun) when is_binary(jid) do
    route(XmppUtils.parse_jid(jid), data, find_recipients_fun)
  end

  def route(jid, data, find_recipients_fun) do
      Lager.debug("Routing #{data}")
      case find_recipients_fun.(jid) do
        [] ->
          {:not_found, jid}
        sessions ->
            Enum.each(sessions,
              fn(session) ->
                Huron.Connection.send_packet(data, session)
              end)
            :ok
      end
  end

end

