defmodule Huron.Router.Bisect do
  require Lager
  @ring_size 128 

  def start_link do
   Agent.start_link(fn ->
    Enum.each(1..@ring_size, 
	fn(p) -> 
    		:bisect_server.start_link(partition_table(p-1), 40, 8)
        end)
   end)
  end

  def register(%{
                    user: user,
                    host: host,
                    resource: resource,
                    socket: socket
                    } = _session) do
    {partition, key} = bisect_key(user, host, resource)
    :bisect_server.insert(partition_table(partition), 
	key,
        port_to_binary(socket)
      )
  end

  def unregister(%{user: user,
                  host: host,
                  resource: resource} = _session) do
    {partition, key} = bisect_key(user, host, resource)
    :bisect_server.delete(partition_table(partition),
	key
	)
  end


  def find_by(jid) do
    case jid do
      {user, host, resource} ->
	{partition, key} = bisect_key(user, host, resource)
        case :bisect_server.get(partition_table(partition), key) do
	  {:ok, :not_found} -> :not_found
	  {:ok, v} -> [v]
	end  
      {user, host} ->
        find_sessions(user, host)
    end
  end

  def session_count do
    Enum.reduce(1..@ring_size, 0, fn(p, acc)-> {:ok, c} = :bisect_server.num_keys(:"session.#{p-1}"); acc + c end)
  end

  def route(jid, data) do
 	  route(jid, data, &__MODULE__.find_by/1)
  end

  def route(jid, data, find_recipients_fun) when is_binary(jid) do
    route(XmppUtils.parse_jid(jid), data, find_recipients_fun)
  end

  def route(jid, data, find_recipients_fun) do
      Lager.debug("Routing #{data}")
      case find_recipients_fun.(jid) do
        :not_found ->
          {:not_found, jid}
        sessions ->
            Enum.each(sessions,
              fn(bin_socket) ->
	        socket = binary_to_port(bin_socket)
		    :ranch_tcp.send(socket, data)
              end)
            :ok
      end
  end

  defp find_sessions(user, host) do
    {partition, partial_key} = bisect_key(user, host)
    anchor = partial_key <> << 0 :: size(160) >>
    find_sessions(partition_table(partition), anchor, partial_key, []) 
  end

  defp find_sessions(sessionTab, full_key, partial_key, acc) do
     case :bisect_server.next(sessionTab, full_key) do
       {:ok, :not_found} -> acc
       {:ok, {<< p_key :: binary - size(20), _rest :: binary>> = k, v}} when p_key == partial_key ->
         find_sessions(sessionTab, k, partial_key, [v | acc])
       _other_key -> acc
     end         
  end

  defp bisect_key(user, host, resource) do
       user_key = :crypto.hash(:sha, :erlang.term_to_binary({host, user}))
       {Huron.CHash.partition(user_key, @ring_size), user_key<>:crypto.hash(:sha, resource)}
  end

  defp bisect_key(user, host) do
       user_key = :crypto.hash(:sha, :erlang.term_to_binary({host, user})) 
       {Huron.CHash.partition(user_key, @ring_size), user_key}
  end


  def port_to_binary(port) do
	'#Port<' ++ x = :erlang.port_to_list(port)
        [p1, p2] = :string.tokens(:string.strip(x, :right, ?>), '.')
        pad_to(:erlang.list_to_integer(p1), 4) <> pad_to(:erlang.list_to_integer(p2), 4)        
  end

  def binary_to_port(binary) do	
	<< _p1 :: size(32), p2 :: size(32) >> = binary
  	n = to_string(Node.self())
	n_len = :erlang.iolist_size(n)
	vsn = :binary.last(:erlang.term_to_binary(self()))
	bin = <<131, 102, 100, n_len :: size(16), n :: binary, p2 :: size(32), vsn :: size(8) >> 
	:erlang.binary_to_term(bin)
  end 

  ## Construct bisect partition table
  defp partition_table(partition) do
     :"session.#{partition}"
  end

  ## Padding integer/binary
  defp pad_to(int, width) when is_integer(int) do
     pad_to(:binary.encode_unsigned(int), width)
  end

  defp pad_to(binary, width) when is_binary(binary) do
     case rem(rem(width - :erlang.size(binary), width), width) do
       0 -> binary
       n -> nz = n*8; <<0 :: size(nz) >> <> binary
     end
  end
end

defmodule Huron.CHash do
  @ringtop trunc(:math.pow(2,160)-1)

  def partition(hash, ring_size) do
    << hash_int  :: size(160) >> = hash
    partition_size = div(@ringtop, ring_size)
    div(hash_int, partition_size)    
  end
end

