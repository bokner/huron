defmodule Huron.Router.Mnesia do
  require Lager

  def start_link do
    Agent.start_link(fn ->
    case Huron.Cluster.cluster? do
        false ->
            :mnesia.create_table(
            :session,
    		[type: :ordered_set,
    		 storage_properties:
    			[ets:
                  [
    				read_concurrency: true,
    				write_concurrency: true
                  ]
    			],
    		 record_name: :session
    		])
    	peer_nodes when is_list(peer_nodes) -> join_mnesia(peer_nodes)
    	end
    end)
  end

  def join_mnesia(peer_nodes) do
    Lager.info("Joining mnesia cluster with #{inspect peer_nodes}...")
    ## Add our node to mnesia on one of the peer nodes
    :rpc.call(hd(peer_nodes), :mnesia, :change_config, [:extra_db_nodes, [:erlang.node]])
    ## Make sure our schema has disc_copies type
    :mnesia.change_table_copy_type(:schema, :erlang.node, :disc_copies)
    ## ... and the same for peer nodes
    for n <- peer_nodes do
        :rpc.call(n, :mnesia, :change_table_copy_type, [:schema, n, :disc_copies])
    end
    ## Copy tables from peer nodes to local node
    Enum.each(:mnesia.system_info(:tables),
        fn(t) -> case :mnesia.table_info(t, :where_to_commit) do
                    [] -> :ok
                    [{_commit_node, type} | _] -> :mnesia.add_table_copy(t, :erlang.node, type)
                 end
        end)
  end

  def register(session) do
  	register(session, :session)
  end


  def register(%{
                    user: user,
                    host: host,
                    resource: resource
                    } = session, sessionTab) do
    :mnesia.dirty_write(sessionTab, {:session,
        {host, user, resource},
        session
      })
  end

  def unregister(session) do
  	unregister(session, :session)
  end

  def unregister(%{user: user,
                  host: host,
                  resource: resource} = _session, sessionTab) do
    :mnesia.dirty_delete(sessionTab, {host, user, resource})
  end


  def find_by(jid, sessionTab) do
    sessions = case jid do
      {user, host, resource} ->
        :ets.lookup(sessionTab, {host, user, resource})
      {user, host} ->
        ## :ets.fun2ms(fn({{jid, _}, session} = obj) -> obj end)
        match = [{{:session, {host, user, :_}, :"$1"}, [], [:"$_"]}]
        :mnesia.dirty_select(sessionTab, match)
    end
    Enum.map(sessions, fn({:session, _key, session}) -> session end)
  end

  def find_by(jid) do
  	find_by(jid, :session)
  end

  def route(session, data) do
       Huron.Connection.send_packet(data, session)
  end

end

