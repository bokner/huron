defmodule Huron.WPoolRouter do
  @pool_name "huron.router.pool"
  @pool_size 256

  def start_link do
    :wpool.start_sup_pool(@pool_name, [workers: @pool_size])
    Huron.Router.start_link
  end

  def register(session) do
   :wpool.cast(@pool_name, {Huron.Router, :register, [session]})
  end

  def unregister(session) do
   :wpool.cast(@pool_name, {Huron.Router, :unregister, [session]})
  end

  def find_by(jid) do
    :wpool.call(@pool_name, {Huron.Router, :find_by, [jid]})
  end

  def route(jid, data) do
    :wpool.cast(@pool_name, {Huron.Router, :route, [jid, data]})
  end
end