defmodule Huron.Router.Syn do
  @moduledoc false
  require Lager

  def start_link do
    {:ok, pid} = Huron.Router.Mnesia.start_link
    :syn.init
    {:ok, pid}
  end

  def register(%{user: user, host: host, resource: resource, pid: pid} = session) do
    :syn.register({user, host, resource},
        pid
    )
    :syn.join({user, host}, pid)
  end

  def unregister(session) do
    :ok
  end

  ## Find session by full jid
  def find_by({_user, _host, _resource} = jid) do
      case :syn.find_by_key(jid) do
        :undefined -> []
        session_pid -> [session_pid]
      end
  end

  ## Find sessions by bare jid
  def find_by({_user, _host} = jid) do
     :syn.get_members(jid)
  end

  def session_count do
    :syn.register_count
  end

  def route(session, data) when is_pid(session) do
    send session, {:route, data}
  end

end
