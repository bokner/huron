defmodule Huron.Cache do
 ## Try to get the data from cache; if not in the cache, use get_fun instead, put the result into cache
 ## for consequent use
 def get(cache_name, key, get_fun) do
   case :cache.get(cache_name, key) do
    :undefined ->
       res = get_fun.(key)
       :ok = :cache.put(cache_name, key, res)
       res
    cached -> cached
   end
 end

 def uncache_session({user, host, resource} = _jid) do
   :cache.remove_(:session_cache, {user, host})
   :cache.remove_(:session_cache, {user, host, resource})
 end

end